<?php
session_start();
$level=0;
if (isset($_SESSION['level']))
{
	$level=$_SESSION['level'];
}
else
{
	header('Location:login.php');
}
?>

<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Akteku</title>
	<meta name="description" content="">
	<meta name="author" content="">

	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link href="css/bootstrap.css" rel="stylesheet" />
	<link href="css/jui.css" rel="stylesheet" />
	
	<link href="css/bsc.css" rel="stylesheet" />
	

		
	<link href="css/animate.css" rel="stylesheet" />
	<script src="js/libs/jquery/jquery-min.js"></script>
	
	<script src="js/libs/highchart/highcharts.js"></script>
	<script src="js/libs/bootstrap/bootstrap.min.js"></script>
	
	
	



    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>





    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
	<script data-main="js/main" src="js/libs/require/require.js"></script>
	
</head>

<body>

<div id='page'></div>

</body>

</html>