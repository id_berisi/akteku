// Author: Thomas Davis <thomasalwyndavis@gmail.com>
// Filename: main.js

// Require.js allows us to configure shortcut alias
// Their usage will become more apparent futher along in the tutorial.
require.config({
  paths: {
    jquery: 'libs/jquery/jquery-2.2.3.min',
    underscore: 'libs/underscore/underscore-min',
    backbone: 'libs/backbone/backbone-min',
	menu:'libs/menu/menu',
	bootstrap: 'libs/bootstrap/bootstrap.min',
    templates: '../templates',
	datatables:'libs/datatables/datatables.min',
	jui: 'libs/jquery/jqueryui',
	'datatables.net':'libs/datatables/jquery.dataTables.min',
	'datatables.net-bs':'libs/datatables/dataTables.bootstrap.min'
  }

});

require([
  // Load our app module and pass it to our definition function
  'app',

], function(App){
  // The "app" dependency is passed in as "App"
  // Again, the other dependencies passed in are not "AMD" therefore don't pass a parameter to this function
  App.initialize();
});
