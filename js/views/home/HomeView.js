define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'menu',
    'collections/home/Collection',
    'models/home/Model',
    'text!templates/home/template.html',
	'jui'
], function($, _, Backbone, Bootstrap, Menu, Collection, Model, Template,Jui) {



    var HomeView = Backbone.View.extend({
        el: $("#page"),
        template: _.template(Template),
        initialize: function() {
            var self = this;

			
            //Kota
            var KotaModel = Backbone.Model.extend({
                defaults: {

                }
            });
            var collectionx = Backbone.Collection.extend({
                model: KotaModel,
                url: "http:./api/table.php?tbl=refkota",
            });

            this.collectionx = new collectionx();
            this.collectionx.fetch({
                reset: true,
                traditional: true,
                success: function(collectionx, response) {
                    $(function() {
                        var lkota = "";
			var optKota = "";
                        for (var x = 0; x < collectionx.length; x++) {
                          
			    optKota +='<option value="'+collectionx.models[x].attributes.id+'">'+ collectionx.models[x].attributes.nama +'</option>'
                        }
					$(".kotakPlh").append(optKota);
					$(".kotakPlh2").append(optKota);

                    });
                    console.log('fetch kota list success');
                },
                error: function(response) {
                    console.log('fetch kota list fail');
                },
            });
			/*
            //User
            var UserModel = Backbone.Model.extend({
                defaults: {

                }
            });
            var colu = Backbone.Collection.extend({
                model: UserModel,
                url: "http:./api/table.php?tbl=datauser",
            });

            this.colu = new colu();
            this.colu.fetch({
                reset: true,
                traditional: true,
                success: function(colu, response) {
                    $(function() {
                        var luser = "";
                        for (var x = 0; x < colu.length; x++) {
                            luser += '<li class="list-group-item">' + colu.models[x].attributes.nama + '</li>';
                        }
                        $(".luser").append(luser);
                    });
                    console.log('fetch user list success');
                },
                error: function(response) {
                    console.log('fetch user list fail');
                },
            }); */

            //Akte
            var AkteModel = Backbone.Model.extend({
                defaults: {

                }
            });
            var coakte = Backbone.Collection.extend({
                model: AkteModel,
                url: "http:./api/table.php?tbl=dtak",
            });

            this.coakte = new coakte();
            this.coakte.fetch({
                reset: true,
                traditional: true,
                success: function(coakte, response) {
                    function getMnt(m) {
                        var d = new Date();
                        var month = new Array();
                        month[1] = "January";
                        month[2] = "February";
                        month[3] = "March";
                        month[4] = "April";
                        month[5] = "May";
                        month[6] = "June";
                        month[7] = "July";
                        month[8] = "August";
                        month[9] = "September";
                        month[10] = "October";
                        month[11] = "November";
                        month[12] = "December";
                        var n = month[m];
                        return n;
                    }
                    $(function() {

                        var lakte = "";
                        for (var x = 0; x < coakte.length; x++) {
							var delbut='<button ty="akte" idku="'+coakte.models[x].attributes.id+'" type="button"  rel="tooltip" title="" class="ahadel btn btn-danger btn-simple btn-xs" data-original-title="Remove"><i class="fa fa-times"></i></button>';
                            lakte += "<li class='list-group-item'><div class='row'><div class='col-md-4'>" + coakte.models[x].attributes.kota + "</div><div class='col-md-5'>" + coakte.models[x].attributes.tahun + " " + getMnt(coakte.models[x].attributes.bulan) + "</div><div class='col-md-2'>" + coakte.models[x].attributes.jumlah + "</div><div col-md-1>"+delbut+"</div></div></li>";
                        }
                        $(".lakte").append(lakte);
                    });
                    console.log('fetch akte list success');
                },
                error: function(response) {
                    console.log('fetch akte list fail');
                },
            });
			
			
			//Kelahiran
            var LahirModel = Backbone.Model.extend({
                defaults: {

                }
            });
            var colahir = Backbone.Collection.extend({
                model: LahirModel,
                url: "http:./api/table.php?tbl=dtl",
            });
			function getMntx(m) {
                        var d = new Date();
                        var month = new Array();
                        month[1] = "January";
                        month[2] = "February";
                        month[3] = "March";
                        month[4] = "April";
                        month[5] = "May";
                        month[6] = "June";
                        month[7] = "July";
                        month[8] = "August";
                        month[9] = "September";
                        month[10] = "October";
                        month[11] = "November";
                        month[12] = "December";
                        var n = month[m];
                        return n;
            }
            this.colahir = new colahir();
            this.colahir.fetch({
                reset: true,
                traditional: true,
                success: function(colahir, response) {
                    
                    $(function() {

                        var lakte = "";
                        for (var x = 0; x < colahir.length; x++) {
							var delbut='<button ty="lahir" idku="'+colahir.models[x].attributes.id+'" type="button"  rel="tooltip" title="" class="ahadel btn btn-danger btn-simple btn-xs" data-original-title="Remove"><i class="fa fa-times"></i></button>';
                            lakte += "<li class='list-group-item'><div class='row'><div class='col-md-4'>" + colahir.models[x].attributes.kota + "</div><div class='col-md-5'>" + colahir.models[x].attributes.tahun + " " + getMntx(colahir.models[x].attributes.bulan) + "</div><div class='col-md-2'>" + colahir.models[x].attributes.jumlah + "</div><div col-md-1>"+delbut+"</div></div></li>";
                        }
                        $(".llahir").append(lakte);
                    });
                    console.log('fetch akte list success');
                },
                error: function(response) {
                    console.log('fetch akte list fail');
                },
            });
			 
			
			//Data tahun
			
				var thnx = Backbone.Model.extend({
                defaults: {

                }
				});
				var cthn = Backbone.Collection.extend({
					model: thnx,
					url: "http:./api/table.php?tbl=thn&wr=thn",
				});
				
				this.cthn = new cthn();
				this.cthn.fetch({
					reset: true,
					traditional: true,
					success: function(cthn, response) {
						$(function() {

							var lakte = "";
							for (var x = 0; x < cthn.length; x++) {

								lakte +="<option value='"+cthn.models[x].attributes.tahun+"'>"+cthn.models[x].attributes.tahun+"</option>"
							}
							$(".plhTh").append(lakte);
							$(".plhTh2").append(lakte);
							$(".plhTh3").append(lakte);
						});
						console.log('fetch akte list success');
					},
					error: function(response) {
						console.log('fetch akte list fail');
					},
				});
			
			
			//Data Semua
            var dall = Backbone.Model.extend({
                defaults: {

                }
            });
            var cdall = Backbone.Collection.extend({
                model: dall,
                url: "http:./api/table.php?tbl=2016&wr=1",
            });

            this.cdall = new cdall();
            this.cdall.fetch({
                reset: true,
                traditional: true,
                success: function(cdall, response) {
                    $(function() {
                        var luser = "";
						var dls=[];
						var lahir=[];
						var akte=[];
                        for (var x = 0; x < cdall.length; x++) {
                            //luser += '<li class="list-group-item">' + cdall.models[x].attributes.nama + '</li>';
								var persen=parseInt(cdall.models[x].attributes.jumlahAkte)/parseInt(cdall.models[x].attributes.jumlahlahir)*100;
								dls.push(cdall.models[x].attributes.nama+' ('+persen+'%)');
								akte.push(parseInt(cdall.models[x].attributes.jumlahAkte));
								lahir.push(parseInt(cdall.models[x].attributes.jumlahlahir));
                        }
						
						
                        $('#sts2').highcharts({
							chart: {
							type: 'column'
								},
							title: {
							text: 'Analisis Data Kelahiran',
							x: -20 //center
								},
							subtitle: {
							text: 'Tahun 2016',
							x: -20
								},
							xAxis: {
							categories: dls,
							crosshair: true
								},
							yAxis: {
							min: 0,
							title: {
							text: 'Jiwa'
									}
								},
							tooltip: {
							valueSuffix: 'Jiwa'
								},
							legend: {
							layout: 'vertical',
							align: 'right',
							verticalAlign: 'middle',
							borderWidth: 0
								},
							series: [{
							name: 'Kelahiran',
							data: lahir

								}, {
							name: 'Akte',
							data: akte

								}]

							});
                    });
                    console.log('fetch dall list success');
                },
                error: function(response) {
                    console.log('fetch dall list fail');
                },
            });
			
			var dall = Backbone.Model.extend({
                defaults: {

                }
            });
            var cdall = Backbone.Collection.extend({
                model: dall,
                url: "http:./api/table.php?tbl=2016&wr=1",
            });

            this.cdall = new cdall();
            this.cdall.fetch({
                reset: true,
                traditional: true,
                success: function(cdall, response) {
                    $(function() {
                        var luser = "";
						var dls=[];
						var lahir=[];
						var akte=[];
						var ak=0;
						var lh=0;
                        for (var x = 0; x < cdall.length; x++) {
                            //luser += '<li class="list-group-item">' + cdall.models[x].attributes.nama + '</li>';
								
								ak +=parseInt(cdall.models[x].attributes.jumlahAkte)
								lh +=parseInt(cdall.models[x].attributes.jumlahlahir)
                        }
						var persen=(ak/lh)*100;
						dls.push('Total ('+persen+'%)');
						akte.push(ak);
						lahir.push(lh);
						
                        $('#sts3').highcharts({
							chart: {
							type: 'column'
								},
							title: {
							text: 'Analisis Data Kelahiran',
							x: -20 //center
								},
							subtitle: {
							text: 'Tahun 2016',
							x: -20
								},
							xAxis: {
							categories: dls,
							crosshair: true
								},
							yAxis: {
							min: 0,
							title: {
							text: 'Jiwa'
									}
								},
							tooltip: {
							valueSuffix: 'Jiwa'
								},
							legend: {
							layout: 'vertical',
							align: 'right',
							verticalAlign: 'middle',
							borderWidth: 0
								},
							series: [{
							name: 'Kelahiran',
							data: lahir

								}, {
							name: 'Akte',
							data: akte

								}]

							});
                    });
                    console.log('fetch dall list success');
                },
                error: function(response) {
                    console.log('fetch dall list fail');
                },
            });

			var call = Backbone.Model.extend({
                defaults: {

                }
            });
            var cdcall = Backbone.Collection.extend({
                model: call,
                url: "http:./api/table.php?tbl=2016&kota=1&wr=2",
            });

            this.cdcall = new cdcall();
			this.cdcall.fetch({
                reset: true,
                traditional: true,
                success: function(cdcall, response) {
                    $(function() {
                        var luser = "";
						var bln=[];
						var lahir=[];
						var akte=[];
                        for (var x = 0; x < cdcall.length; x++) {
                            //luser += '<li class="list-group-item">' + cdall.models[x].attributes.nama + '</li>';
								var persen=parseInt(cdcall.models[x].attributes.jumlahAkte)/parseInt(cdcall.models[x].attributes.jumlahLahir)*100;
								bln.push(getMntx(cdcall.models[x].attributes.BulanAkte)+' ('+persen+'%)');
								akte.push(parseInt(cdcall.models[x].attributes.jumlahAkte));
								lahir.push(parseInt(cdcall.models[x].attributes.jumlahLahir));
                        }
						
						
                         $('#sts').highcharts({
							title: {
								text: 'Analisis Data Kelahiran',
								x: -20 //center
							},
							subtitle: {
								text: "Tahun "+$(".plhTh2").val(),
								x: -20
							},
							xAxis: {
								categories: bln
							},
							yAxis: {
								title: {
									text: 'Jiwa'
								},
								plotLines: [{
									value: 0,
									width: 1,
									color: '#808080'
								}]
							},
							tooltip: {
								valueSuffix: 'Jiwa'
							},
							legend: {
								layout: 'vertical',
								align: 'right',
								verticalAlign: 'middle',
								borderWidth: 0
							},
							series: [{
								name: 'Kelahiran',
								data: akte
							}, {
								name: 'Pembuatan Akte',
								data: lahir
							}]

						});
                    });
                    console.log('fetch dall list success');
                },
                error: function(response) {
                    console.log('fetch dall list fail');
                },
            });
        },

        render: function() {
            $('.sidebar-nav li a').removeClass('act');
            $('.sidebar-nav li a[href="#"]').addClass('act');
			
            this.$el.html(this.template());
           
			
			$('#tanggalKelahiran').datepicker({dateFormat:"yy-mm-dd"});
			$('#tanggalAkte').datepicker({dateFormat:"yy-mm-dd"});


        },
		
		events: {
			'change .plhTh': 'choseYear',
			'change .plhTh3': 'choseYear2',
			'change .plhTh2':'choseX',
			'change .kotakPlh':'choseX',
			'click .addakte':'modalAkte',
			'click .addlahir':'modalLahir',
			'click #subkel':'insertKelahiran',
			'click #subak':'insertAkte',
			'click .ahadel':'delL'
		},
		
		delL:function(e){
			var idk=e.currentTarget.attributes.idku.nodeValue;
			var tbls='';
			if(e.currentTarget.attributes.ty.nodeValue=="akte"){
				tbls='akte';
			}
			else{
				tbls='lahir';
			}
			
			if (confirm("Anda yakin ingin menghapus data?") == true) {
				$.ajax({
					type: 'POST',
					url: 'api/deletedata.php',
					 data: {
							iditem: idk,
							tbl: tbls
						},
					success: function(data) {
						$('#notif').fadeIn('slow');
						$('#notif').html(data);
						alert(data);
						if (data=='Succes delete data!')
						{
							location.reload();
						}
					}
				})
			} 
			
		},
		
		insertKelahiran(){
			var jumlahAkte=$("#jumlahKelahiran").val();
			var kotaAkte=$("#kotaKelahiran").val();
			var tanggalAkte=$("#tanggalKelahiran").val();
			$.ajax({
				type: 'POST',
				url: 'api/datastkelahiran_insert.php',
				 data: {
						jumlah: jumlahAkte,
						kota: kotaAkte,
						tanggal: tanggalAkte
					},
				success: function(data) {
					$('#notif').fadeIn('slow');
					$('#notif').html(data);
					alert(data);
					if (data=='Succes add data!')
					{
						location.reload();
					}
				}
			})
		},
		
		insertAkte(){
			var jumlahAkte=$("#jumlahAkte").val();
			var kotaAkte=$("#kotaAkte").val();
			var tanggalAkte=$("#tanggalAkte").val();
			$.ajax({
				type: 'POST',
				url: 'api/datastakte_insert.php',
				 data: {
						jumlah: jumlahAkte,
						kota: kotaAkte,
						tanggal: tanggalAkte
					},
				success: function(data) {
					$('#notif').fadeIn('slow');
					$('#notif').html(data);
					alert(data);
					if (data=='Succes add data!')
					{
						location.reload();
					}
					//$('#tbldatastkelahiran').load('api/datastkelahiran_insert.php');
					
				}
			})
		},
		
		modalAkte: function () {
			$('#modalAkte').modal('show')
		},
		
		modalLahir: function () {
			$('#modalLahir').modal('show')
		},
		choseX: function () {
			var year=$('.plhTh2').val();
			var kota=$('.kotakPlh').val();
			var call = Backbone.Model.extend({
                defaults: {

                }
            });
            var cdcall = Backbone.Collection.extend({
                model: call,
                url: "http:./api/table.php?tbl="+year+"&kota="+kota+"&wr=2",
            });

            this.cdcall = new cdcall();
			this.cdcall.fetch({
                reset: true,
                traditional: true,
                success: function(cdcall, response) {
					
					function getMntx(m) {
                        var d = new Date();
                        var month = new Array();
                        month[1] = "January";
                        month[2] = "February";
                        month[3] = "March";
                        month[4] = "April";
                        month[5] = "May";
                        month[6] = "June";
                        month[7] = "July";
                        month[8] = "August";
                        month[9] = "September";
                        month[10] = "October";
                        month[11] = "November";
                        month[12] = "December";
                        var n = month[m];
                        return n;
            }
					
                    $(function() {
                        var luser = "";
						var bln=[];
						var lahir=[];
						var akte=[];
                        for (var x = 0; x < cdcall.length; x++) {
                            //luser += '<li class="list-group-item">' + cdall.models[x].attributes.nama + '</li>';
								var persen=parseInt(cdcall.models[x].attributes.jumlahAkte)/parseInt(cdcall.models[x].attributes.jumlahLahir)*100;
								bln.push(getMntx(cdcall.models[x].attributes.BulanAkte)+' ('+persen+'%)');
								bln.push(cdcall.models[x].attributes.BulanAkte);
								akte.push(parseInt(cdcall.models[x].attributes.jumlahAkte));
								lahir.push(parseInt(cdcall.models[x].attributes.jumlahLahir));
                        }
						
						
                         $('#sts').highcharts({
							title: {
								text: 'Analisis Data Kelahiran',
								x: -20 //center
							},
							subtitle: {
								text: "Tahun "+$(".plhTh2").val(),
								x: -20
							},
							xAxis: {
								categories: bln
							},
							yAxis: {
								title: {
									text: 'Jiwa'
								},
								plotLines: [{
									value: 0,
									width: 1,
									color: '#808080'
								}]
							},
							tooltip: {
								valueSuffix: 'Jiwa'
							},
							legend: {
								layout: 'vertical',
								align: 'right',
								verticalAlign: 'middle',
								borderWidth: 0
							},
							series: [{
								name: 'Kelahiran',
								data: akte
							}, {
								name: 'Pembuatan Akte',
								data: lahir
							}]

						});
                    });
                    console.log('fetch dall list success');
                },
                error: function(response) {
                    console.log('fetch dall list fail');
                },
            });
		},

		
		choseYear: function () {
			//Data Semua
			var year=$('.plhTh').val();
            var dall = Backbone.Model.extend({
                defaults: {

                }
            });
            var cdall = Backbone.Collection.extend({
                model: dall,
                url: "http:./api/table.php?tbl="+year+"&wr=1",
            });

            this.cdall = new cdall();
            this.cdall.fetch({
                reset: true,
                traditional: true,
                success: function(cdall, response) {
                    $(function() {
                        var luser = "";
						var dls=[];
						var lahir=[];
						var akte=[];
                        for (var x = 0; x < cdall.length; x++) {
                            //luser += '<li class="list-group-item">' + cdall.models[x].attributes.nama + '</li>';
								var persen=parseInt(cdall.models[x].attributes.jumlahAkte)/parseInt(cdall.models[x].attributes.jumlahlahir)*100;
								dls.push(cdall.models[x].attributes.nama+' ('+persen+'%)');
								akte.push(parseInt(cdall.models[x].attributes.jumlahAkte));
								lahir.push(parseInt(cdall.models[x].attributes.jumlahlahir));
                        }
						
						
                        $('#sts2').highcharts({
							chart: {
							type: 'column'
								},
							title: {
							text: 'Analisis Data Kelahiran',
							x: -20 //center
								},
							subtitle: {
							text: 'Tahun 2016',
							x: -20
								},
							xAxis: {
							categories: dls,
							crosshair: true
								},
							yAxis: {
							min: 0,
							title: {
							text: 'Jiwa'
									}
								},
							tooltip: {
							valueSuffix: 'Jiwa'
								},
							legend: {
							layout: 'vertical',
							align: 'right',
							verticalAlign: 'middle',
							borderWidth: 0
								},
							series: [{
							name: 'Kelahiran',
							data: lahir

								}, {
							name: 'Akte',
							data: akte

								}]

							});
                    });
                    console.log('fetch dall list success');
                },
                error: function(response) {
                    console.log('fetch dall list fail');
                },
            });
		},
		
		choseYear2: function () {
			//Data Semua
			var year=$('.plhTh3').val();
            var dall = Backbone.Model.extend({
                defaults: {

                }
            });
            var cdall = Backbone.Collection.extend({
                model: dall,
                url: "http:./api/table.php?tbl="+year+"&wr=1",
            });

            this.cdall = new cdall();
            this.cdall.fetch({
                reset: true,
                traditional: true,
                success: function(cdall, response) {
                    $(function() {
                        var luser = "";
						var dls=[];
						var lahir=[];
						var akte=[];
						var ak=0;
						var lh=0;
                        for (var x = 0; x < cdall.length; x++) {
                            //luser += '<li class="list-group-item">' + cdall.models[x].attributes.nama + '</li>';
								
								ak +=parseInt(cdall.models[x].attributes.jumlahAkte)
								lh +=parseInt(cdall.models[x].attributes.jumlahlahir)
                        }
						var persen=(ak/lh)*100;
						dls.push('Total ('+persen+'%)');
						akte.push(ak);
						lahir.push(lh);
						
						
                        $('#sts3').highcharts({
							chart: {
							type: 'column'
								},
							title: {
							text: 'Analisis Data Kelahiran',
							x: -20 //center
								},
							subtitle: {
							text: 'Tahun 2016',
							x: -20
								},
							xAxis: {
							categories: dls,
							crosshair: true
								},
							yAxis: {
							min: 0,
							title: {
							text: 'Jiwa'
									}
								},
							tooltip: {
							valueSuffix: 'Jiwa'
								},
							legend: {
							layout: 'vertical',
							align: 'right',
							verticalAlign: 'middle',
							borderWidth: 0
								},
							series: [{
							name: 'Kelahiran',
							data: lahir

								}, {
							name: 'Akte',
							data: akte

								}]

							});
                    });
                    console.log('fetch dall list success');
                },
                error: function(response) {
                    console.log('fetch dall list fail');
                },
            });
		},




    });

    return HomeView;

});