define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'menu',
    'text!templates/visimisi/template.html',
	'jui'
], function($, _, Backbone, Bootstrap, Menu, Template,Jui) {



    var HomeView = Backbone.View.extend({
        el: $("#page"),
        template: _.template(Template),
		initialize: function(){
			this.render();
		},
     
		render: function() {
            this.$el.html(this.template());
        }
    });

    return HomeView;

});