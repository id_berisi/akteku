define([
  'jquery',
  'underscore',
  'backbone',
  'models/customer/AddCustomerModel'
], function($, _, Backbone, AddCustomerModel){
  var ProjectsCollection = Backbone.Collection.extend({
    model: AddCustomerModel,
    
    initialize: function(){

      //this.add([project0, project1, project2, project3, project4]);

    }

  });
 
  return ProjectsCollection;
});
