define([
  'underscore',
  'backbone',
  'models/customer/CustomerModel'
], function(_, Backbone, CustomerModel){

   var collection = Backbone.Collection.extend({
        model: CustomerModel,
        url: "http:./api/user.json",
    });

    return collection; 

});