define([
  'underscore',
  'backbone',
  'models/customer/details/pods/Model'
], function(_, Backbone, Model){

   var collection = Backbone.Collection.extend({
        model: Model,
        url: "http:./api/customer_pods.json",
    });

    return collection; 

});