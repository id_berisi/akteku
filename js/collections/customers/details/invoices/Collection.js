define([
  'underscore',
  'backbone',
  'models/customer/details/invoices/Model'
], function(_, Backbone, Model){

   var collection = Backbone.Collection.extend({
        model: Model,
        url: "http:./api/customer_invoices.json",
    });

    return collection; 

});