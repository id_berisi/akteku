define([
  'underscore',
  'backbone',
  'models/customer/details/orders/Model'
], function(_, Backbone, Model){

   var collection = Backbone.Collection.extend({
        model: Model,
        url: "http:./api/customer_orders.json",
    });

    return collection; 

});