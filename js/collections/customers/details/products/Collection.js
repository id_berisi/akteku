define([
  'underscore',
  'backbone',
  'models/customer/details/products/Model'
], function(_, Backbone, Model){

   var collection = Backbone.Collection.extend({
        model: Model,
        url: "http:./api/customer_product.json",
    });

    return collection; 

});