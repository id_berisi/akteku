define([
  'underscore',
  'backbone',
  'models/configurations/CustomerGroupModel'
], function(_, Backbone, CustomerModel){

   var collection = Backbone.Collection.extend({
        model: CustomerModel,
        url: "http:./api/customer_group.json",
    });

    return collection; 

});