define([
  'underscore',
  'backbone',
  'models/configurations/StateModel'
], function(_, Backbone, CustomerModel){

   var collection = Backbone.Collection.extend({
        model: CustomerModel,
        url: "http:./api/state.json",
    });

    return collection; 

});