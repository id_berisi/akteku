$(document).ready(function() {

    $('.close-alert').click(function() {
        $('.errorAlert').addClass('animated fadeOut');
    });

    $('#notification').click(function() {
        $('#notification:after').after('display', 'none');
    });
	
	
	
	
});


function setAlertMessage(message) {
    $(".alert-msg").text(message);
}


function getStatusBadge()
{
	$('.order-status').each(function() {
        var status=$(this).text();
		var array= status.split('');
		$(this).text("");
		for (var i=0;i<array.length;i++)
		{
			var act="order-deact";
			if(array[i]==1)
			{
				act="order-act";
			}
			if (i==0)
			{
				$(this).append("<span class='"+act+"'>Put</span>");
			}
			else if (i==1)
			{
				$(this).append("<span class='"+act+"'>Pick</span>");
			}
			
			else if (i==2)
			{
				$(this).append("<span class='"+act+"'>Pack</span>");
			}
			
			else if (i==3)
			{
				$(this).append("<span class='"+act+"'>WIP</span>");
			}
			
			else
			{
				$(this).append("<span class='"+act+"'>Ship</span>");
			}
		}
    });
}


function changePage(table, pageNumber) {
    $(table + ' tbody tr').hide();
    var itemTable = (pageNumber - 1) * 10;
    for (var i = itemTable; i < itemTable + 10; i++) {
        $(table + ' tbody tr:nth-child(' + (i + 1) + ')').show();
    }

    $('#select-page').val(pageNumber);
    $('.back').attr("page", pageNumber);
    $('.next').attr("page", pageNumber);
}

function tableData(table) {
    var rowCount = $(table + '  tr').length;
    var pageCount = Math.ceil(rowCount / 10);

    for (var i = 1; i <= pageCount - 1; i++) {
        $('#select-page').append($('<option>', {
            value: i,
            text: i
        }));
    }

    $('.page-number-from').text("1");
    $('.page-number-to').text(pageCount - 1);
    $('.last').attr("page", pageCount - 1);


    $('.last').click(function() {
        changePage(table, $(this).attr("page"));
    });

    $('.first').click(function() {
        changePage(table, $(this).attr("page"));

    });

    $('.back').click(function() {
        var page = parseInt($(this).attr("page"));
        if (page > 1) {
            changePage(table, page - 1);
        }
    });

    $('.next').click(function() {
        var page = parseInt($(this).attr("page"));
        if (page < pageCount - 1) {
            changePage(table, page + 1);
        }
    });

    $('#select-page').change(function() {
		if($(this).val()!=0)
		{
			changePage(table, $(this).val());
			$('.back').attr("page", $(this).val());
			$('.next').attr("page", $(this).val());
		}
        
    });

    $('#sort-by').change(function() {
		if($(this).val()!=0)sortTable(table + ' ', $(this).val());
    });

    var thCount = 1;
    $(table + '  thead tr:nth-child(2) td').each(function() {
        if ($(this).text() != 'Details') {
            $('#sort-by').append($('<option>', {
                value: thCount,
                text: $(this).text()
            }));
            thCount += 1;
        }
    });
    $(table + '  tbody tr:nth-child(n+11)').hide();
}


function sortTable(table, row) {
    var rows = $(table + ' tbody  tr').get();
    if (row > 0) {
        rows.sort(function(a, b) {
            var A = $(a).children('td').eq(row - 1).text().toUpperCase();
            var B = $(b).children('td').eq(row - 1).text().toUpperCase();

            if (A < B) {
                return -1;
            }

            if (A > B) {
                return 1;
            }

            return 0;

        });

        $.each(rows, function(index, row) {
            $(table).children('tbody').append(row);
        });

        changePage(table, 1);
    }
}

function back()
{
	
}

