-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 23 Jun 2016 pada 11.36
-- Versi Server: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aktekelahiran`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `datastakte`
--

CREATE TABLE `datastakte` (
  `id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `kota` int(11) NOT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `datastakte`
--

INSERT INTO `datastakte` (`id`, `jumlah`, `tanggal`, `kota`, `user`) VALUES
(1, 100, '2016-06-01', 1, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `datastkelahiran`
--

CREATE TABLE `datastkelahiran` (
  `id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `kota` int(11) NOT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `datastkelahiran`
--

INSERT INTO `datastkelahiran` (`id`, `jumlah`, `tanggal`, `kota`, `user`) VALUES
(1, 150, '2016-06-01', 1, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `datauser`
--

CREATE TABLE `datauser` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `datauser`
--

INSERT INTO `datauser` (`id`, `nama`, `email`, `pass`, `level`) VALUES
(1, 'admin', 'admin@akte.com', '1234', 1),
(2, 'op', 'op@aktre.com', '1234', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `refkota`
--

CREATE TABLE `refkota` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `refkota`
--

INSERT INTO `refkota` (`id`, `nama`) VALUES
(1, 'Tanjung Pinang'),
(2, 'Batam');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `datastakte`
--
ALTER TABLE `datastakte`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datastkelahiran`
--
ALTER TABLE `datastkelahiran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datauser`
--
ALTER TABLE `datauser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refkota`
--
ALTER TABLE `refkota`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `datastakte`
--
ALTER TABLE `datastakte`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `datastkelahiran`
--
ALTER TABLE `datastkelahiran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `datauser`
--
ALTER TABLE `datauser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `refkota`
--
ALTER TABLE `refkota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
