define([
  'underscore',
  'backbone',
], function(_, Backbone) {

  var model = Backbone.Model.extend({
        initialize:function(){
        },
        url: "http://localhost:1234/logisticalbb/api/customer_invoices.json",
        methodUrl: {
            'create': '/Configuration/DepartmentAdd',
            'update': '/Configuration/DepartmentUpdate',
            'delete': '/Configuration/DepartmentDelete'
        },
        idAttribute: "Id",
        defaults: {

        },
        sync: function (method, model, options) {
            if (model.methodUrl && model.methodUrl[method.toLowerCase()]) {
                options = options || {};
                options.url = model.methodUrl[method.toLowerCase()];
            }
            Backbone.sync(method, model, options);
        }
    });

    return model;

});

