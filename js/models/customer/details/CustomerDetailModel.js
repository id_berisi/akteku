define([
  'underscore',
  'backbone',
], function(_, Backbone) {

  var CustomerModel = Backbone.Model.extend({});

  return CustomerModel;

});
