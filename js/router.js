// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'views/home/HomeView',
  'views/visimisi/VisiMisiView',
], function($,
	_, 
	Backbone, 
	HomeView,
	VisiMisi
	) {
  
  var AppRouter = Backbone.Router.extend({
    routes: {
      // Define some URL routes
      'visimisi': 'showVisiMisi', 
      // Default
      '*actions': 'defaultAction'
    }
  });
  
  var initialize = function(){

    var app_router = new AppRouter;
    
    app_router.on('route:showVisiMisi', function(){
		var visiMisi = new VisiMisi();
        visiMisi.render();
    });
	
    app_router.on('route:defaultAction', function (actions) {
        var homeView = new HomeView();
        homeView.render();
    });
	
	
	
	

   

    Backbone.history.start();
  };
  return { 
    initialize: initialize
  };
});
