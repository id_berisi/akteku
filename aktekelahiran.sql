-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02 Jul 2016 pada 19.41
-- Versi Server: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aktekelahiran`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `dall`
--
CREATE TABLE `dall` (
`id` int(11)
,`jumlah` int(11)
,`tanggal` date
,`kota_id` int(11)
,`kota` varchar(100)
,`code` varchar(1)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `datastakte`
--

CREATE TABLE `datastakte` (
  `id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `kota` int(11) NOT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `datastakte`
--

INSERT INTO `datastakte` (`id`, `jumlah`, `tanggal`, `kota`, `user`) VALUES
(1, 100, '2016-06-01', 1, 2),
(2, 200, '2016-07-01', 2, 1),
(3, 500, '2015-11-05', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `datastkelahiran`
--

CREATE TABLE `datastkelahiran` (
  `id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `kota` int(11) NOT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `datastkelahiran`
--

INSERT INTO `datastkelahiran` (`id`, `jumlah`, `tanggal`, `kota`, `user`) VALUES
(1, 150, '2016-06-01', 1, 2),
(2, 30, '2016-07-01', 1, 1),
(3, 140, '2016-07-01', 2, 1),
(5, 400, '2015-11-05', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `datauser`
--

CREATE TABLE `datauser` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `datauser`
--

INSERT INTO `datauser` (`id`, `nama`, `email`, `pass`, `level`) VALUES
(1, 'admin', 'admin@akte.com', '1234', 1),
(2, 'op', 'op@aktre.com', '1234', 2);

-- --------------------------------------------------------

--
-- Stand-in structure for view `dtak`
--
CREATE TABLE `dtak` (
`id` int(11)
,`jumlah` int(11)
,`tanggal` date
,`kota` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `dtl`
--
CREATE TABLE `dtl` (
`id` int(11)
,`jumlah` int(11)
,`tanggal` date
,`tahun` int(4)
,`bulan` int(2)
,`kota` varchar(100)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `refkota`
--

CREATE TABLE `refkota` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `refkota`
--

INSERT INTO `refkota` (`id`, `nama`) VALUES
(1, 'Tanjung Pinang'),
(2, 'Batam');

-- --------------------------------------------------------

--
-- Struktur untuk view `dall`
--
DROP TABLE IF EXISTS `dall`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dall`  AS  select `datastakte`.`id` AS `id`,`datastakte`.`jumlah` AS `jumlah`,`datastakte`.`tanggal` AS `tanggal`,`refkota`.`id` AS `kota_id`,`refkota`.`nama` AS `kota`,'A' AS `code` from (`datastakte` join `refkota` on((`datastakte`.`kota` = `refkota`.`id`))) union select `datastkelahiran`.`id` AS `id`,`datastkelahiran`.`jumlah` AS `jumlah`,`datastkelahiran`.`tanggal` AS `tanggal`,`refkota`.`id` AS `kota_id`,`refkota`.`nama` AS `kota`,'K' AS `code` from (`datastkelahiran` join `refkota` on((`datastkelahiran`.`kota` = `refkota`.`id`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `dtak`
--
DROP TABLE IF EXISTS `dtak`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dtak`  AS  select `datastakte`.`id` AS `id`,`datastakte`.`jumlah` AS `jumlah`,`datastakte`.`tanggal` AS `tanggal`,`refkota`.`nama` AS `kota` from (`datastakte` join `refkota` on((`datastakte`.`kota` = `refkota`.`id`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `dtl`
--
DROP TABLE IF EXISTS `dtl`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dtl`  AS  select `datastkelahiran`.`id` AS `id`,`datastkelahiran`.`jumlah` AS `jumlah`,`datastkelahiran`.`tanggal` AS `tanggal`,year(`datastkelahiran`.`tanggal`) AS `tahun`,month(`datastkelahiran`.`tanggal`) AS `bulan`,`refkota`.`nama` AS `kota` from (`datastkelahiran` join `refkota` on((`datastkelahiran`.`kota` = `refkota`.`id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `datastakte`
--
ALTER TABLE `datastakte`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datastkelahiran`
--
ALTER TABLE `datastkelahiran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datauser`
--
ALTER TABLE `datauser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refkota`
--
ALTER TABLE `refkota`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `datastakte`
--
ALTER TABLE `datastakte`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `datastkelahiran`
--
ALTER TABLE `datastkelahiran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `datauser`
--
ALTER TABLE `datauser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `refkota`
--
ALTER TABLE `refkota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
